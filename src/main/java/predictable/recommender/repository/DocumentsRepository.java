package predictable.recommender.repository;

import predictable.recommender.Document;
import predictable.recommender.connection.DatabaseConnection;
import predictable.recommender.exceptions.DbException;
import predictable.recommender.exceptions.RepositoryException;

import java.util.ArrayList;
import java.util.List;

public class DocumentsRepository extends Repository<Document> {

    public DocumentsRepository(DatabaseConnection connection) {
        super(Document.class, connection);
    }

    /**
     * Get documents with id greater than given id
     *
     * @param id threshold id
     * @return documents with id greater than param id
     * @throws RepositoryException
     */
    public List<Document> getFromId(Integer id, Integer limit) throws RepositoryException {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();

        try {
            sql.append("SELECT \"id\", \"headline\", \"text\", \"created\", \"updated\"");
            sql.append("FROM ").append(connection.getTableName(Document.class));

            sql.append("WHERE \"id\" > ?");
            params.add(id);

            sql.append("ORDER BY \"id\"");

            sql.append("LIMIT ?");
            params.add(limit);

            sql.append(";");
        } catch(DbException e) {
            throw new RepositoryException("Cannot create SQL query", e);
        }

        List<Document> documents;
        try {
            documents = connection.fetchAllRows(sql.toString(), params.toArray(), Document.class);
        } catch (DbException e) {
            throw new RepositoryException("Cannot load documents", e);
        }

        return documents;
    }

}
