package predictable.recommender.data;

class DataSupervisorThread extends Thread {

    /** milliseconds*/
    private static final int FETCH_INTERVAL = 500;

    private DataSupervisor dataSupervisor;

    DataSupervisorThread(DataSupervisor dataSupervisor) {
        this.dataSupervisor = dataSupervisor;
    }

    @Override
    public void run() {
        while(true) {
            Integer updatedCount = dataSupervisor.updateDocumentsDataset();

            if(updatedCount == 0) {
                try {
                    sleep(FETCH_INTERVAL);
                } catch (InterruptedException e) {
                    // suppress interruptions
                }
            }
        }
    }

}
