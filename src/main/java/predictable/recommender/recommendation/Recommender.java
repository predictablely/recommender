package predictable.recommender.recommendation;

import predictable.recommender.Document;
import predictable.recommender.exceptions.RecommenderException;
import predictable.recommender.exceptions.SimilarityException;
import predictable.recommender.index.Index;
import predictable.recommender.interactions.InteractionsVector;
import predictable.recommender.similarity.ISimilarityVector;
import predictable.recommender.similarity.SimilarityHandler;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class Recommender {

    private Index index;

    public Recommender() {
        this.index = new Index();
    }

    /**
     *
     * @param d document added into index
     * @return this
     */
    public Recommender addDocument(Document d) {
        try {
            this.index.insert(d);
        } catch (Exception e) {
            // TODO does this need to be handled somehow? (document already is in the index)
        }
        return this;
    }

    /**
     * TODO add similarity handler as generic to method parameters
     *
     * @param iv vector of interactions
     * @return list of recommended items
     * @throws RecommenderException
     */
    public List<RecommendedItem> recommend(InteractionsVector iv) throws RecommenderException {
        // compute similarity to all documents
        ISimilarityVector sv;
        try {
            SimilarityHandler sh = new SimilarityHandler(this.index);
            sv = sh.computeSimilarities(iv.getDocuments());
        } catch(SimilarityException e) {
            throw new RecommenderException("One of documents not contained in the index");
        }

        // just create the response structure
        List<RecommendedItem> recommendedItems = new LinkedList<>();
        for(Map.Entry<Document, Double> entry : sv.entrySet()) {
            Document document = entry.getKey();
            Double score = entry.getValue();
            recommendedItems.add(new RecommendedItem(document, score));
        }

        // sort by score
        Collections.sort(recommendedItems, Collections.reverseOrder());
        return recommendedItems;
    }

    public Recommender recountIdf() {
        for(Document document : this.index.getDocuments().values()) {
            Double length = document.getLength(index);
        }
        return this;
    }

    public List<RecommendedItem> recommend(Document document) throws Exception {
        InteractionsVector iv = new InteractionsVector()
                .addDocument(document, 1.0)
        ;
        return this.recommend(iv);
    }

    /*public List<RecommendedItem> recommend(APIDocument document) throws Exception {
        if( ! this.index.hasDocument(document)) {
            throw new Exception("APIDocument not found in the index");
        }

        SimilarityHandler sh = new SimilarityHandler();
        ISimilarityVector sv = sh.computeSimilarities();

        List<RecommendedItem> response = new LinkedList<>();
        return response;
    }*/

}
