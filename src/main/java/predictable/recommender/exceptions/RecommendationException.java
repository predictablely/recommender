package predictable.recommender.exceptions;

public class RecommendationException extends Exception {

    public RecommendationException() {
    }

    public RecommendationException(String message) {
        super(message);
    }

    public RecommendationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecommendationException(Throwable cause) {
        super(cause);
    }

    public RecommendationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
