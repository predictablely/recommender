package predictable.recommender.repository;

import predictable.recommender.Document;
import predictable.recommender.connection.DatabaseConnection;

public class InteractionsRepository extends Repository<Document> {

    public InteractionsRepository(DatabaseConnection connection) {
        super(Document.class, connection);
    }

}
