package predictable.recommender.exceptions;

public class RecommenderException extends Exception {

    public RecommenderException() {
    }

    public RecommenderException(String message) {
        super(message);
    }

    public RecommenderException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecommenderException(Throwable cause) {
        super(cause);
    }

    public RecommenderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
