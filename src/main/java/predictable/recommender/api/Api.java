package predictable.recommender.api;

import com.google.gson.Gson;
import predictable.recommender.RecommenderDaemon;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Facade class for API calls
 *
 */
@Path("/")
public class Api {

    @Path("/recount-lengths")
    @GET
    @Produces("application/json")
    public String recountLength() {
        long time1= System.nanoTime();
        RecommenderDaemon.getRecommender().recountIdf();
        long timeSpent = System.nanoTime()-time1;
        long usedMemory  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("recommended in "+(timeSpent/1000000)+"ms and used "+(usedMemory/(1024*1024))+"mb of memory");
        return "";
    }

    @Path("recommend/{id}")
    @GET
    @Produces("application/json")
    public String recommend(@PathParam("id") Integer id) throws Exception {
        RecommendationApi api = new RecommendationApi();
        return api.getItemRecommendation(id);

        /*
        List<RecommendedItem> recommendedItems; // = new LinkedList<>();
        long time1= System.nanoTime();

        Recommender recommender = RecommenderDaemon.getRecommender();
        // recommendation
        recommendedItems = recommender.recommend(75896);
        //recommendedItems = recommenderDaemon.getRecommender().recommend(3751992);

        long timeSpent = System.nanoTime()-time1;
        Runtime runtime = Runtime.getRuntime();
        long usedMemory  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("recommended in "+(timeSpent/1000000)+"ms and used "+(usedMemory/(1024*1024))+"mb of memory");

        List<APIRecommendedItem> apiResponse = new LinkedList<>();
        Integer limit = Math.min(20, recommendedItems.size());
        for(RecommendedItem ri : recommendedItems.subList(0, limit)) {
            apiResponse.add(new APIRecommendedItem(ri));
        }
        return new Gson().toJson(apiResponse);
        */

        /*
        RecommendationApi r = new RecommendationApi();

        r.addItem("item_1");
        r.addItem("item_2");
        r.addItem("item_3");

        System.out.println("recommend");

        //Recommender r = new Recommender();

        return new Gson().toJson(r);
        */
    }

}
