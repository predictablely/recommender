package predictable.recommender;

import edu.stanford.nlp.ling.CoreAnnotations.*;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Parser {

    public List<String> parse(String text) {
        return Arrays.asList(text.split("\\s+"));
    }

    public static void main(String[] args) {
        /*
        MaxentTagger tagger = new MaxentTagger("edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger");
        String str = "A tearful Obama announces new executive actions on gun control";
        String taggedStr = tagger.tagString(str);
        System.out.println(taggedStr);

        Reader reader = new StringReader(str);
        Sentence sentence = Sentence.
        */

        //PropertiesUtils props = new PropertiesUtils();
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, sentiment");
        StanfordCoreNLP stanford = new StanfordCoreNLP(props);

        String text = "A tearful Obama announces new executive actions on gun control. Angela Merkel is doing nothing in Berlin. This is shit. This is pretty good. This is damn good! Fuck society!";
        //String text = "Different version of the Aboriginal orthodoxy are found in the post-imperial era wherever indigenous peoples existing as minorities in settler societies are made aware that they are not alone, that their past treatment and future aspirations are shared by other indigenous peoples around the globe.";

        Annotation document = new Annotation(text);

        stanford.annotate(document);

        List<CoreMap> sentences = document.get(SentencesAnnotation.class);

        for(CoreMap sentence : sentences) {
            Tree tree = sentence. get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);
            int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
            System.out.println(sentiment);
        }

        for(CoreMap sentence : sentences) {
            // traversing the words in the current sentence
            // a CoreLabel is a CoreMap with additional token-specific methods
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                // this is the text of the token
                String word = token.get(TextAnnotation.class);
                // token?
                String tok = token.get(WordSenseAnnotation.class);
                // this is the POS tag of the token
                String pos = token.get(PartOfSpeechAnnotation.class);
                // lemma?
                String lemma = token.get(LemmaAnnotation.class);
                // this is the NER label of the token
                String ne = token.get(NamedEntityTagAnnotation.class);
                // sentiment of the token
                //String sentiment = token.get(Sentim SentimentAnnotation.class);

                System.out.println(".");
            }
        }

        // hash tagy - porovnat se slovníkem

    }

}
