package predictable.recommender.data;

import predictable.recommender.Document;
import predictable.recommender.exceptions.RepositoryException;
import predictable.recommender.recommendation.Recommender;
import predictable.recommender.repository.DocumentsRepository;
import predictable.recommender.repository.InteractionsRepository;
import predictable.recommender.repository.UsersRepository;

import java.util.List;

public class DataSupervisor {

    public static final int FETCH_LIMIT = 100;

    protected InteractionsRepository interactionsRepository;

    protected Recommender recommender;
    protected DocumentsRepository documentsRepository;

    protected UsersRepository usersRepository;

    protected Integer lastLoadedId;

    public DataSupervisor(
            Recommender recommender,
            DocumentsRepository documentsRepository,
            UsersRepository usersRepository,
            InteractionsRepository interactionsRepository
    ) {
        this.recommender = recommender;
        this.documentsRepository = documentsRepository;
        this.usersRepository = usersRepository;
        this.interactionsRepository = interactionsRepository;

        this.lastLoadedId = 0;

        // start thread for background data update
        this.startLoadingThread();
    }

    /**
     * Load new data from the database
     *
     * @return number of documents loaded by this update
     */
    public Integer updateDocumentsDataset() {
        Integer documentsLoaded = 0;
        try {
            List<Document> documents = documentsRepository.getFromId(lastLoadedId, FETCH_LIMIT);
            for(Document document : documents) {
                recommender.addDocument(document);
                if(document.getId() > this.lastLoadedId) {
                    this.lastLoadedId = document.getId();
                }
                documentsLoaded++;
            }
        } catch(RepositoryException e) {
            // suppress errors
            e.printStackTrace();
        }
        return documentsLoaded;
    }

    /**
     * Start thread for background data update
     * Thread calls this class for the update itself
     *
     * @return this
     */
    protected DataSupervisor startLoadingThread() {
        Thread t = new DataSupervisorThread(this);
        t.start();
        return this;
    }

}
