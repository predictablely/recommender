package predictable.recommender;

import predictable.recommender.connection.DatabaseConnection;
import predictable.recommender.connection.PostgresDatabaseConnection;
import predictable.recommender.data.DataSupervisor;
import predictable.recommender.recommendation.Recommender;
import predictable.recommender.repository.DocumentsRepository;
import predictable.recommender.repository.InteractionsRepository;
import predictable.recommender.repository.UsersRepository;

/**
 * Singleton instance of recommender
 *
 */
public class RecommenderDaemon {

    private static Recommender recommender;
    private static DataSupervisor dataSupervisor;

    public static void init() {
        // init recommender
        recommender = new Recommender();
        // init data supervisor
        DatabaseConnection databaseConnection = new PostgresDatabaseConnection();
        DocumentsRepository documentsRepository = new DocumentsRepository(databaseConnection);
        UsersRepository usersRepository = new UsersRepository(databaseConnection);
        InteractionsRepository interactionsRepository = new InteractionsRepository(databaseConnection);
        dataSupervisor = new DataSupervisor(recommender, documentsRepository, usersRepository, interactionsRepository);
    }

    public static Recommender getRecommender() {
        return recommender;
    }

    public static DataSupervisor getDataSupervisor() {
        return dataSupervisor;
    }

    /**
     * alias for relearn()
     *
     * @return this
     *
    public RecommenderDaemon learn() {
    return this.relearn();
    }*/

    /**
     * insert all documents and index them
     *
     * @return this
     *
    public RecommenderDaemon relearn() {
        this.isLearned = false;
        this.recommender = new Recommender();

        Date since = new DateTime().minusHours(5).toDate();

        this.updateData(since);

        this.isLearned = true;
        System.out.println("Learned");

        return this;
    }*/

    /**
     *
     * @param since date from which updating should start
     * @return dummy
     *
    private Boolean updateData(Date since) {
        Date until = new Date();
        while(true) {
            try {
                // load documents from a particular time period and save them
                //throw new Exception("nejde podle času (ztrácí se data), musí se to brát podle posledního načteného id (PK)");
                List<Document> documents = this.loadDocuments(since, until);
                System.out.println("New documents loaded "+documents.size());
                for(Document document : documents) {
                    this.getRecommender().addDocument(document);
                }
                // set vars for next round (every 1 sec)
                since = until;
                until = new Date();
                Thread.sleep(DATA_CHECK_PERIOD);
            } catch(InterruptedException e) {
                e.printStackTrace();
                break;
            } catch(Exception e) {
                e.printStackTrace();
                break;
            }
        }
        return true;
    }*/

    /**
     *
     * @return
     *
    private List<Document> loadDocuments(Date since, Date until) {
        List<Document> documents = new ArrayList<>();
        try {
            Postgres p = new Postgres();
            documents = p.getDocuments(since, until);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return documents;
    }*/

}
