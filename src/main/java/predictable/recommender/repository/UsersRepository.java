package predictable.recommender.repository;

import predictable.recommender.Document;
import predictable.recommender.connection.DatabaseConnection;

public class UsersRepository extends Repository<Document> {

    public UsersRepository(DatabaseConnection connection) {
        super(Document.class, connection);
    }

}
