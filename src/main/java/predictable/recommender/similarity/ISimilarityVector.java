package predictable.recommender.similarity;

import predictable.recommender.Document;

import java.util.Map;
import java.util.Set;

/**
 *
 */
public interface ISimilarityVector {

    public ISimilarityVector addSimilarity(Document document, Double similarity);

    public ISimilarityVector addNormalization(Document document, Double normalization);

    public SimilarityVector setSelfNormalization(Double normalization);

    public Set<Map.Entry<Document, Double>> entrySet();

}
