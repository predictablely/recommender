package predictable.recommender;

import java.util.HashMap;
import java.util.Map;

public class Dictionary {

    private Map<String, Integer> wordsMap;
    private Map<Integer, String> idsMap;
    private Integer counter;

    public Dictionary() {
        wordsMap = new HashMap<>();
        idsMap = new HashMap<>();
        counter = 0;
    }

    /**
     * return integer id of word and insert it if word is not in the dictionary
     *
     * @param word
     * @return word id
     */
    public Integer getId(String word) {
        if(wordsMap.get(word) == null) {
            wordsMap.put(word, this.counter);
            idsMap.put(this.counter, word);
            this.counter += 1;
        }
        return wordsMap.get(word);
    }

    /**
     * get real word from id
     *
     * @param id
     * @return word string or null
     */
    public String getWord(Integer id) {
        return idsMap.get(id);
    }

}
