package predictable.recommender.interactions;

import predictable.recommender.Document;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class InteractionsVector {

    private List<Document> documents;
    private List<Double> weights;
    private List<InteractionType> types;

    public InteractionsVector() {
        documents = new ArrayList<Document>();
        weights = new ArrayList<Double>();
        types = new ArrayList<InteractionType>();
    }

    /**
     *
     * @param document
     * @param weight
     * @return
     */
    public InteractionsVector addDocument(Document document, Double weight) {
        documents.add(document);
        weights.add(weight);
        return this;
    }

    /**
     *
     * @return
     */
    public List<Document> getDocuments() {
        return documents;
    }

}
