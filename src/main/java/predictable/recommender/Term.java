package predictable.recommender;

import predictable.recommender.index.Index;

/**
 *
 */
public class Term {

    private Integer word;
    private Integer occurrences;
    private Double frequency;

    //private Double weight;

    public Term(Integer word) {
        this.word = word;
        this.occurrences = 0;
    }

    public Term(Integer word, Index index) {
        this.word = word;
        this.occurrences = 0;
    }

    public void addOccurrences(Integer occurrences, Integer documentWords) {
        this.occurrences += occurrences;
        this.frequency = this.occurrences/(double)documentWords;
    }

    public Term merge(Term term) {
        if(term != null) {
            this.occurrences += term.occurrences;
            this.frequency += term.frequency;
            //this.weight = Math.max(this.weight, term.weight);
        }
        return this;
    }

    public Double getFrequency() {
        return frequency;
    }

    public Double getInverseDocumentFrequency(Index index) {
        Double idf = 0.0;
        idf = index.getSize()
                / (double)index
                    .getOccurrences(this)
                    .size();
        return idf;
    }

    @Override
    public int hashCode() {
        return this.word;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null || ! (o instanceof Term)) {
            return false;
        }
        Term t = (Term) o;
        return this.word == t.word;
    }

}
