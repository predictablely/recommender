package predictable.recommender.index;

import predictable.recommender.Document;
import predictable.recommender.Term;

/**
 * TODO all the class can be deleted to save space, just consider if weight makes sense anywhere and consider moving occurrences and frequency here
 *
 */
public class TermOccurrence {

    private Term term;
    private Document document;
    //private Double weight; -- more general than just occurrences

    public TermOccurrence(Term term, Document document) {
        this.term = term;
        this.document = document;
    }

    public Term getTerm() {
        return term;
    }

    public Document getDocument() {
        return document;
    }
}
