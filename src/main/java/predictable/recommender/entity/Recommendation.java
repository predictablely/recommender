package predictable.recommender.entity;

import java.util.LinkedList;
import java.util.List;

public class Recommendation {

    private List<String> items;

    public Recommendation() {
        this.items = new LinkedList<>();
    }

    public List<String> getItems() {
        return items;
    }

    public Recommendation addItem(String item) {
        this.items.add(item);
        return this;
    }

}
