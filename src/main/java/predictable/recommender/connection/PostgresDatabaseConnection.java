package predictable.recommender.connection;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import predictable.recommender.exceptions.DbException;
import predictable.recommender.utils.db.DbUtils;
import predictable.recommender.utils.db.AnnotationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class PostgresDatabaseConnection implements DatabaseConnection {

    private static final String DB_DRIVER = "org.postgresql.Driver";
    private static final String DB_CONNECTION = "jdbc:postgresql://192.168.99.100:7772/recommender";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "pass";

    private static Connection dbConnection;

    private static Connection getDBConnection() throws DbException {
        if(dbConnection == null) {
            try {
                Class.forName(DB_DRIVER);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new DbException(e);
            }

            try {
                dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
            } catch (SQLException e) {
                throw new DbException(e);
            }
        }

        return dbConnection;
    }

    public String getTableName(Class clazz) throws DbException {
        String tableName;
        String schemaName;
        try {
            tableName = DbUtils.getJPATableName(clazz);
            schemaName = DbUtils.getJPASchemaName(clazz);
        } catch (AnnotationException e) {
            throw new DbException(e.getMessage(), e);
        }
        return String.format("\"%s\".\"%s\"", schemaName, tableName);
    }

    @Override
    public <T> List<T> fetchAllRows(String sql, Object[] params, Class<T> type) throws DbException {
        List<T> results;
        try {
            QueryRunner run = new QueryRunner();
            ResultSetHandler<List<T>> h = new BeanListHandler<>(type, new BasicRowProcessor( new BeanProcessor(DbUtils.getJPAColumnsToPropertiesMapping(type))));
            results = run.query(getDBConnection(), sql, h, params);
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return results;
    }

    @Override
    public <T> T fetchOneRow(String sql, Object[] params, Class<T> type) throws DbException {
        T results;
        try {
            QueryRunner run = new QueryRunner();
            ResultSetHandler<T> h = new BeanHandler<>(type, new BasicRowProcessor( new BeanProcessor(DbUtils.getJPAColumnsToPropertiesMapping(type))));
            results = run.query(getDBConnection(), sql, h, params);
        } catch (SQLException e) {
            throw new DbException(e);
        }
        return results;
    }

}
