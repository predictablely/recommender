package predictable.recommender.connection;

import predictable.recommender.exceptions.DbException;

import java.util.List;

public interface DatabaseConnection {

    String getTableName(Class clazz) throws DbException;

    <T> List<T> fetchAllRows(String sql, Object[] params, Class<T> type) throws DbException;

    <T> T fetchOneRow(String sql, Object[] params, Class<T> type) throws DbException;

}
