package predictable.recommender.exceptions;

public class SimilarityException extends Exception {

    public SimilarityException() {
    }

    public SimilarityException(String message) {
        super(message);
    }

    public SimilarityException(String message, Throwable cause) {
        super(message, cause);
    }

    public SimilarityException(Throwable cause) {
        super(cause);
    }

    public SimilarityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
