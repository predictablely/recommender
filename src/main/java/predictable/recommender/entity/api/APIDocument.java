package predictable.recommender.entity.api;

import predictable.recommender.Document;

/**
 *
 */
public class APIDocument {

    public Integer id;
    public String headline;
    public String text;

    public APIDocument(Document document) {
        this.id = document.getId();
        this.headline = document.getHeadline();
        this.text = document.getText();
    }

}
