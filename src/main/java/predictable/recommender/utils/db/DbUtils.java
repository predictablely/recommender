package predictable.recommender.utils.db;

import predictable.recommender.utils.db.AnnotationException;
import predictable.recommender.utils.db.Column;
import predictable.recommender.utils.db.Table;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.HashMap;

public class DbUtils {

    public static String getJPATableName(Class clazz) throws AnnotationException {
        Table table = (Table) clazz.getAnnotation(Table.class);
        if(table == null) {
            throw new AnnotationException("Missing Table annotation");
        }
        return table.name();
    }

    public static String getJPASchemaName(Class clazz) throws AnnotationException {
        Table table = (Table) clazz.getAnnotation(Table.class);
        if(table == null) {
            throw new AnnotationException("Missing Table annotation");
        }
        return table.schema();
    }

    public static Map<String, String> getJPAColumnsToPropertiesMapping(Class clazz) {
        Map<String, String> columnsToPropertiesMapping = new HashMap<>();

        for (Field field : clazz.getDeclaredFields()) {
            Column column = field.getAnnotation(Column.class);
            if (column != null && ! column.name().equals("")) {
                columnsToPropertiesMapping.put(column.name(), field.getName());
            }
        }

        return columnsToPropertiesMapping;
    }

}
