package predictable.recommender;

import predictable.recommender.index.DocumentProperties;
import predictable.recommender.index.Index;
import predictable.recommender.utils.db.Column;
import predictable.recommender.utils.db.Table;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Table(name = "articles", schema = "predictable")
public class Document implements Serializable {

    @Column(name = "id")
    private Integer id;

    @Column(name = "crawled")
    private Integer crawled;

    @Column(name = "headline")
    private String headline;

    @Column(name = "text")
    private String text;

    @Column(name = "created")
    private Date created;

    @Column(name = "updated")
    private Date updated;

    private Map<Term, Term> terms;
    private Double length;

    private DocumentProperties properties;

    public Document() {
        this.length = 0.0;
        this.terms = new HashMap<>();
    }

    public Document(Integer id, Integer crawled, String headline, String text) {
        this();

        this.id = id;
        this.crawled = crawled;
        this.headline = headline;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public DocumentProperties getProperties() {
        return properties;
    }

    /**
     * Parse the document using parser and encode to representation using dictionary
     *
     * @param parser parser used for text processing
     * @param dictionary dictionary of tokens
     * @return this
     */
    public Document parse(Parser parser, Dictionary dictionary) {
        // TODO use proper parser to create terms list
        List<String> tokens = parser.parse(this.text);
        for(String token : tokens) {
            // get id from dictionary
            Integer wordId = dictionary.getId(token);
            // add to terms map and increase occurrences counter
            Term newTerm = new Term(wordId);
            Term term = this.terms.get(newTerm);
            if(term == null) {
                term = newTerm;
                this.terms.put(term, term);
            }
            term.addOccurrences(1, tokens.size());
        }

        // TODO get correct document properties
        //this.properties = new DocumentProperties(this);

        return this;
    }

    /**
     * Parse terms from document text and headline using parser
     *
     * @param parser parser used for text processing
     * @param dictionary dictionary of tokens
     * @param numWordGroups number of tokens in generated groups
     * @return this
     */
    public Document parse(Parser parser, Dictionary dictionary, int numWordGroups) {
        return this.parse(parser, dictionary);
    }

    /**
     * get list of terms in the document
     *
     * @return terms
     */
    public Map<Term, Term> getTerms() {
        return terms;
    }

    public Double getLength(Index index) {
        Double sumQuad = 0.0;
        Integer length = 0;
        for(Term term : this.terms.values()) {
            length += 1;
            sumQuad += term.getFrequency() / term.getInverseDocumentFrequency(index);
        }
        return sumQuad / length;
    }


    /**
     *  Merge two documents into a single one
     *  just sum terms occurrences
     *
     * @param document to merge with
     * @return this
     */
    public Document merge(Document document) {
        for(Term term : document.getTerms().values()) {
            Term actTerm = this.terms.get(term);
            // term is not in the document - create new term
            if(actTerm == null) {
                actTerm = term;
            // term is already in this document - merge them
            } else {
                actTerm.merge(term);
            }
            this.terms.put(actTerm, actTerm);
        }

        this.length += document.length;

        return this;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null || ! (o instanceof Document)) {
            return false;
        }
        Document t = (Document) o;
        return this.id.equals(t.id);
    }

}
