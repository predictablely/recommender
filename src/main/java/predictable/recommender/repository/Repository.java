package predictable.recommender.repository;

import predictable.recommender.connection.DatabaseConnection;
import predictable.recommender.exceptions.DbException;
import predictable.recommender.exceptions.RepositoryException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Repository<T> {

    protected Class<T> type;
    protected DatabaseConnection connection;

    Repository(Class<T> type, DatabaseConnection connection) {
        this.type = type;
        this.connection = connection;
    }

    public T getById(Integer id) throws RepositoryException {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();

        try {
            sql.append("SELECT *");
            sql.append("FROM ").append(connection.getTableName(type));

            sql.append("WHERE \"id\" = ?");
            params.add(id);

            sql.append(";");
        } catch(DbException e) {
            throw new RepositoryException("Cannot create SQL query", e);
        }

        T result;
        try {
            result = connection.fetchOneRow(sql.toString(), params.toArray(), type);
        } catch (DbException e) {
            throw new RepositoryException("Cannot load documents", e);
        }

        return result;
    }

    public List<T> getByIds(Integer[] ids) throws RepositoryException {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();

        List<T> results = new ArrayList<>();
        if(ids.length > 0) {
            try {
                sql.append("SELECT *");
                sql.append("FROM ").append(connection.getTableName(type));

                sql.append("WHERE \"id\" IN (");
                for(int i = 0; i < ids.length; i++) {
                    sql.append(i == 0 ? "?" : ",?");
                    params.add(ids[i]);
                }
                sql.append(")");

                sql.append(";");
            } catch(DbException e) {
                throw new RepositoryException("Cannot create SQL query", e);
            }

            try {
                results = connection.fetchAllRows(sql.toString(), params.toArray(), type);
            } catch (DbException e) {
                throw new RepositoryException("Cannot load documents", e);
            }
        }

        return results;
    }

    public List<T> getAll() throws RepositoryException {
        StringBuilder sql = new StringBuilder();

        try {
            sql.append("SELECT *");
            sql.append("FROM ").append(connection.getTableName(type));
            sql.append(";");
        } catch(DbException e) {
            throw new RepositoryException("Cannot create SQL query", e);
        }

        List<T> results;
        try {
            //Object[] params = {};
            results = connection.fetchAllRows(sql.toString(), null, type);
        } catch (DbException e) {
            throw new RepositoryException("Cannot load documents", e);
        }

        return results;
    }

}
