package predictable.recommender.similarity;

import predictable.recommender.Document;
import predictable.recommender.Term;
import predictable.recommender.exceptions.IndexException;
import predictable.recommender.exceptions.SimilarityException;
import predictable.recommender.index.Index;
import predictable.recommender.index.TermOccurrence;

import java.util.*;

public class SimilarityHandler {

    private Index index;

    public SimilarityHandler(Index index) {
        this.index = index;
    }

    /**
     * Merge all input documents into a single one and then compute similarity to this document
     *
     * @param documents list of documents to compute similarity to
     * @return similarity to all documents in the index
     * @throws SimilarityException
     */
    public ISimilarityVector computeSimilarities(List<Document> documents) throws SimilarityException {
        Document mergedDocument = new Document(null, null, null, null);
        try {
            for(Document document : documents) {
                Document indexedDocument = this.index.getDocument(document);
                mergedDocument.merge(indexedDocument);
            }
        } catch(IndexException e) {
            throw new SimilarityException("One of documents not contained in the index");
        }
        return this.computeSimilarities(mergedDocument);
    }

    /**
     * Compute vector of similarities to all documents in the index
     *
     * @param document document to which compute all similarities
     * @return vector of similarities
     */
    public ISimilarityVector computeSimilarities(Document document) {
        Integer indexSize = index.getSize();
        ISimilarityVector sv = new SimilarityVector(indexSize);

        // normalization by length of the actual document
        Double selfNormalization = 0.0;

        // for every term find its occurrences using the inverse index
        for(Term term : document.getTerms().values()) {
            Integer termDocs = index.getOccurrences(term).size();
            Integer docs = index.getSize();
            Double idf_2 = Math.pow(Math.log10(docs / (double)termDocs), 2);
            // for every occurrence add its piece of similarity and normalization (length) to the document
            for(TermOccurrence occurrence : index.getOccurrences(term)) {
                Double similarity = term.getFrequency() * occurrence.getTerm().getFrequency() * idf_2;
                Double normalization = occurrence.getTerm().getFrequency() * occurrence.getTerm().getFrequency() * idf_2;
                sv.addSimilarity(occurrence.getDocument(), similarity);
                sv.addNormalization(occurrence.getDocument(), normalization);
            }
            selfNormalization += term.getFrequency() * term.getFrequency() * idf_2;
        }

        sv.setSelfNormalization(selfNormalization);

        return sv;
    }

}
