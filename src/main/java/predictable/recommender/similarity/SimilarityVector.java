package predictable.recommender.similarity;

import predictable.recommender.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * similarity vector of document to set of documents
 *
 * TODO refactor similarityVector to Double[]
 */
public class SimilarityVector implements ISimilarityVector {

    private Integer length;

    private Map<Document, Double> similarityVector;
    private Map<Document, Double> normalizationVector;
    private Double selfNormalization;
    private Map<Document, Double> normalizedSimilarityVector;
    private Boolean normalized;


    public SimilarityVector(Integer length) {
        this.length = length;
        this.similarityVector = new HashMap<>(length);
        this.normalizationVector = new HashMap<>(length);
        this.selfNormalization = 0.0;
        this.normalizedSimilarityVector = new HashMap<>(length);
        this.normalized = false;
    }

    /**
     *
     * @param document
     * @param similarity
     * @return
     */
    public SimilarityVector addSimilarity(Document document, Double similarity) {
        Double actSimilarity = this.similarityVector.get(document);
        if(actSimilarity == null) {
            this.similarityVector.put(document, similarity);
        } else {
            this.similarityVector.put(document, actSimilarity + similarity);
        }
        return this;
    }

    /**
     *
     * @param normalization
     * @return
     */
    public SimilarityVector setSelfNormalization(Double normalization) {
        this.selfNormalization = normalization;
        return this;
    }

    /**
     *
     * @param document
     * @param normalization
     * @return
     */
    public SimilarityVector addNormalization(Document document, Double normalization) {
        Double actNormalization = this.normalizationVector.get(document);
        if(actNormalization == null) {
            this.normalizationVector.put(document, normalization);
        } else {
            this.normalizationVector.put(document, actNormalization + normalization);
        }
        return this;
    }

    /**
     * normalize similarity by a normalization vector
     *
     * @return
     * @throws Exception
     */
    private SimilarityVector normalize() {
        Double selfNormalization = Math.sqrt(this.selfNormalization);
        for(Document document : this.similarityVector.keySet()) {
            Double similarity = this.similarityVector.get(document);
            Double normalization = this.normalizationVector.get(document);
            Double normalizedSimilarity = similarity / (Math.sqrt(normalization) * selfNormalization);
            this.normalizedSimilarityVector.put(document, normalizedSimilarity);
        }
        this.normalized = true;
        return this;
    }

    /**
     *
     * @return
     */
    public Set<Map.Entry<Document, Double>> entrySet() {
        if( ! this.normalized) {
            this.normalize();
        }
        return this.normalizedSimilarityVector.entrySet();
    }
}
