package predictable.recommender.api;

import com.google.gson.Gson;
import predictable.recommender.Document;
import predictable.recommender.RecommenderDaemon;
import predictable.recommender.connection.PostgresDatabaseConnection;
import predictable.recommender.entity.api.APIRecommendedItem;
import predictable.recommender.exceptions.RecommendationException;
import predictable.recommender.exceptions.RepositoryException;
import predictable.recommender.recommendation.RecommendedItem;
import predictable.recommender.recommendation.Recommender;
import predictable.recommender.repository.DocumentsRepository;

import java.util.LinkedList;
import java.util.List;

class RecommendationApi {

    /**
     * API for item recommendation response
     * Check that document exists and calculate list of recommended items
     *
     * @param id of the document
     * @return list of recommendations
     * @throws RecommendationException
     */
    String getItemRecommendation(Integer id) throws RecommendationException {
        long time1= System.nanoTime();

        // validate input
        PostgresDatabaseConnection postgresDatabaseConnection = new PostgresDatabaseConnection();
        DocumentsRepository documentsRepository = new DocumentsRepository(postgresDatabaseConnection);
        Document document;
        try {
            document = documentsRepository.getById(id);
        } catch (RepositoryException e) {
            throw new RecommendationException("Document does not exist", e);
        }

        List<RecommendedItem> recommendedItems;
        // recommendation
        Recommender recommender = RecommenderDaemon.getRecommender();
        try {
            recommendedItems = recommender.recommend(document);
        } catch (Exception e) {
            throw new RecommendationException("Cannot load recommendations", e);
        }

        long timeSpent = System.nanoTime()-time1;
        long usedMemory  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("recommended in "+(timeSpent/1000000)+"ms and used "+(usedMemory/(1024*1024))+"mb of memory");

        List<APIRecommendedItem> apiResponse = new LinkedList<>();
        Integer limit = Math.min(20, recommendedItems.size());
        for(RecommendedItem ri : recommendedItems.subList(0, limit)) {
            apiResponse.add(new APIRecommendedItem(ri));
        }
        return new Gson().toJson(apiResponse);
    }
}
