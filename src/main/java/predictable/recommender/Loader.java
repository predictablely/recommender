package predictable.recommender;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;
import predictable.recommender.connection.DatabaseConnection;
import predictable.recommender.connection.PostgresDatabaseConnection;
import predictable.recommender.exceptions.RepositoryException;
import predictable.recommender.repository.DocumentsRepository;
import predictable.recommender.utils.CommandParse;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class Loader {

    public static void testMain(String[] args) {

        Date input = new Date();
        LocalDateTime date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        ZonedDateTime zdt = ZonedDateTime.now(ZoneId.of("Europe/Prague"));
        System.out.println(zdt.toString());

        Duration d = Duration.ZERO.plusMinutes(2);
        Period p = Period.ZERO.plusDays(12); //Period.parse("P12D");
        zdt = zdt.plus(p).plus(d);
        System.out.println(zdt.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));

        DatabaseConnection databaseConnection = new PostgresDatabaseConnection();
        DocumentsRepository documentRepository = new DocumentsRepository(databaseConnection);
        try {
            List<Document> documents = documentRepository.getFromId(2, 10);
            System.out.println(documents);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        // init daemon
    }

    public static void main(String[] args) throws IOException {
        try {
            // init recommender daemon
            RecommenderDaemon.init();
            // start API server
            CommandParse cmd = parseInput(args);
            startServer(cmd.getFlagValue("-ip"), cmd.getFlagValue("-port"));
        } catch(IOException e) {
            System.out.println("Error while starting the server");
            e.printStackTrace();
        }

        //APIDocument d0 = new APIDocument(0, 120, "Interacted document", "Obama is a president king of Obama");
        //APIDocument d0 = new APIDocument(0, 120, "Interacted document", "Obama");

        /*
        Recommender r = new Recommender()
                //.addDocument(d0)
                //.addDocument(new APIDocument(1, 123, "Obama hurts", "Obama is a bad guy who hurts nations"))
                //.addDocument(new APIDocument(2, 124, "Title 2", "President Obama is a bad guy"))
                //.addDocument(new APIDocument(3, 125, "Title 3", "Hillary CLinton is a president candidate"))
                //.addDocument(new APIDocument(4, 126, "Title 4", "No match text"))
                //.addDocument(new APIDocument(5, 127, "Title 5", "Obama and Hillary can win it all"))
                //.addDocument(new APIDocument(6, 128, "Title 6", "Obama"))

                //.addDocument(documents.get(0))
                //.addDocument(documents.get(1))
                //.addDocument(documents.get(2))
                //.addDocument(documents.get(3))
                //.addDocument(documents.get(4))
        ;

        for(APIDocument document : documents) {
            r.addDocument(document);
        }

        InteractionsVector iv = new InteractionsVector()
                //.addDocument(d0, 1.0)
                .addDocument(documents.get(0), 1.0)
                .addDocument(documents.get(1), 1.0)
                .addDocument(documents.get(2), 1.0)
                .addDocument(documents.get(3), 1.0)
                .addDocument(documents.get(4), 1.0)
        ;

        try {
            long time1= System.nanoTime();
            List<RecommendedItem> recommendations = r.recommend(iv);
            long time2 = System.nanoTime();

            long timeSpent = time2-time1;

            Runtime runtime = Runtime.getRuntime();
            long usedMemory  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

            System.out.println("recommended in "+(timeSpent/1000000)+"ms and used "+(usedMemory/(1024*1024))+"mb of memory");
            System.out.println(".");

        } catch (Exception e) {
            e.printStackTrace();
        }
        */

    }

    private static void startServer(String ip, String port) throws IOException {
        if(ip == null) {
            ip = "127.0.0.1";
        }

        if(port == null) {
            port = "8080";
        }

        String address = "http://"+ip+":"+port+"/";

        HttpServer server = HttpServerFactory.create(address);
        server.start();
    }

    private static CommandParse parseInput(String[] args) {
        CommandParse cmd = new CommandParse();
        cmd.saveFlagValue("-port");
        cmd.saveFlagValue("-ip");
        //cmd.saveFlagValue("-log");
        //cmd.saveFlagValue("-filedb");
        //cmd.saveFlagValue("-dbURI");
        //cmd.saveFlagValue("-dbUsername");
        //cmd.saveFlagValue("-dbPassword");
        //cmd.saveFlagValue("-dbName");
        cmd.parse(args);
        return cmd;
    }

}
