package predictable.recommender.entity.api;

import predictable.recommender.recommendation.RecommendedItem;

/**
 *
 */
public class APIRecommendedItem {

    APIDocument document;
    Double score;

    public APIRecommendedItem(RecommendedItem recommendedItem) {
        this.document = new APIDocument(recommendedItem.getDocument());
        this.score = recommendedItem.getScore();
    }
}
