package predictable.recommender.recommendation;

import predictable.recommender.Document;

/**
 *
 */
public class RecommendedItem implements Comparable {

    private Document document;
    private Double score;

    public RecommendedItem(Document document, Double score) {
        this.document = document;
        this.score = score;
    }

    public Document getDocument() {
        return document;
    }

    public Double getScore() {
        return score;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof RecommendedItem) {
            RecommendedItem c = (RecommendedItem)o;
            return this.score.compareTo(c.score);
        }
        return -1;
    }

}
