package predictable.recommender.index;

import predictable.recommender.Dictionary;
import predictable.recommender.Document;
import predictable.recommender.Parser;
import predictable.recommender.Term;
import predictable.recommender.exceptions.IndexException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Inverted index
 */
public class Index {

    private Map<Term, List<TermOccurrence>> terms; // TODO maybe could be just a list of documents
    private Map<Document, Document> documents;

    private Dictionary dictionary;
    private Parser parser;

    public Index() {
        this.terms = new HashMap<>();
        this.documents = new HashMap<>();

        this.dictionary = new Dictionary();
        this.parser = new Parser();
    }

    /**
     * insert document into index
     *
     * @param document to insert
     * @return this
     */
    public Index insert(Document document) throws Exception {
        if(this.documents.get(document) != null) {
            // TODO handle this
            throw new Exception("APIDocument is already in the index");
        }
        // parse document to terms using the parser
        document.parse(this.parser, this.dictionary);
        // store document terms
        for(Term term : document.getTerms().values()) {
            TermOccurrence to = new TermOccurrence(term, document);
            List<TermOccurrence> termList = this.terms.get(term);
            if(termList == null) {
                termList = new LinkedList<>();
                this.terms.put(term, termList);
            }
            termList.add(to);
        }
        // store document properties
        this.documents.put(document, document);

        return this;
    }

    /**
     * check if document is contained in the index
     *
     * @param document to check
     * @return is contained
     */
    public boolean hasDocument(Document document) {
        return this.documents.get(document) != null;
    }

    /**
     * Get indexed document by document that can have only id set in its attributes
     *
     * @param document used as the key
     * @return indexed document
     * @throws IndexException
     */
    public Document getDocument(Document document) throws IndexException {
        Document indexedDocument = this.documents.get(document);
        if(indexedDocument == null) {
            throw new IndexException("APIDocument not contained in the index");
        }
        return indexedDocument;
    }

    /**
     * get list of documents in index
     *
     * @return list of document in index
     */
    public Map<Document, Document> getDocuments() {
        return documents;
    }

    /**
     * @return number of documents in the index
     */
    public Integer getSize() {
        return this.documents.size();
    }

    /**
     * get list of occurrence of the given term
     *
     * @return list of occurrences
     */
    public List<TermOccurrence> getOccurrences(Term term) {
        return this.terms.get(term);
    }
}
